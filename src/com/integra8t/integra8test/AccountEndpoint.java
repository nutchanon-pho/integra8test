package com.integra8t.integra8test;

import java.util.List;

import javax.inject.Named;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.integra8t.integra8test.entity.Account;

@Api(name = "accountendpoint", version = "v1", clientIds = { Constants.WEB_CLIENT_ID,
		Constants.ANDROID_CLIENT_ID }, audiences = { Constants.ANDROID_AUDIENCE })
public class AccountEndpoint {

	@ApiMethod(name = "accountendpoint.getaccountinfobulk", httpMethod = "POST")
	public Account getAccountInfoBulk(@Named("accountIdList") List<String> accountIdList) {
		Account testAccount = new Account("hello","1","2","3");
		return testAccount;
	}
}

