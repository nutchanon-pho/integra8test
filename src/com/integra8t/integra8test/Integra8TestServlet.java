package com.integra8t.integra8test;

import java.io.IOException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class Integra8TestServlet extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		resp.setContentType("text/plain");
		resp.getWriter().println("Hello, world");
	}
}
