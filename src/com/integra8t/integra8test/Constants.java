package com.integra8t.integra8test;

public class Constants {
	public static final String WEB_CLIENT_ID = "940330998484-lf2rumm7mdd8b8pjqlvlosl4si6a9n1j.apps.googleusercontent.com";
	public static final String ANDROID_CLIENT_ID = "940330998484-ffr588lhtsk0jq4olf73e0citra22ecs.apps.googleusercontent.com";
	public static final String IOS_CLIENT_ID = "replace this with your iOS client ID";
	public static final String ANDROID_AUDIENCE = WEB_CLIENT_ID;
}
